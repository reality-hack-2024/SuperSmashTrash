using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AppearTrigger : MonoBehaviour
{
    public float appearTime = 2.0f;
    public float heightDelta = 0.4f;
    public float fracComplete = 0f;

    public GameObject ready;
    public AudioSource readyAudio;
    public GameObject set;
    public AudioSource setAudio;
    public GameObject go;
    public AudioSource goAudio;

    public GameObject nextLevelButton;

    private float startTime;
    private Vector3 startPos;
    private Vector3 endScale;

    public static UnityEvent OnAnimationFinished = new();

    public void Start()
    {
        //StartAnimation();
        TrashSpawner.Instance.OnLevelChange.AddListener(enableNextLvlButton);
    }

    public void enableNextLvlButton()
    {
        nextLevelButton.SetActive(true);
    }

    public void StartAnimation()
    {
        StartCoroutine(Animate());
    }
    private IEnumerator Animate()
    {
        fracComplete = 0;
        ready.SetActive(true);
        readyAudio.Play();
        startTime = Time.time;
        startPos = ready.transform.position;
        endScale = ready.transform.localScale;
        while (fracComplete < 1)
        {
            fracComplete = (Time.time - startTime) / appearTime;
            ready.transform.localScale = Vector3.Slerp(new Vector3(0, 0, 0), endScale, fracComplete);
            ready.transform.localPosition = Vector3.Slerp(new Vector3(startPos.x, startPos.y, startPos.z), new Vector3(startPos.x, startPos.y + heightDelta, startPos.z), fracComplete);
            yield return null;
        }
        ready.SetActive(false);
        startPos = set.transform.position;
        endScale = set.transform.localScale;
        set.SetActive(true);
        setAudio.Play();
        startTime = Time.time;
        fracComplete = 0;
        while (fracComplete < 1)
        {
            fracComplete = (Time.time - startTime) / appearTime;
            set.transform.localScale = Vector3.Slerp(new Vector3(0, 0, 0), endScale, fracComplete);
            set.transform.localPosition = Vector3.Slerp(new Vector3(startPos.x, startPos.y, startPos.z), new Vector3(startPos.x, startPos.y + heightDelta, startPos.z), fracComplete);

            yield return null;
        }
        set.SetActive(false);
        startPos = go.transform.position;
        endScale = go.transform.localScale;
        go.SetActive(true);
        goAudio.Play();
        startTime = Time.time;
        fracComplete = 0;
        while (fracComplete < 1)
        {
            fracComplete = (Time.time - startTime) / appearTime;
            go.transform.localScale = Vector3.Slerp(new Vector3(0, 0, 0), endScale, fracComplete);
            go.transform.localPosition = Vector3.Slerp(new Vector3(startPos.x, startPos.y, startPos.z), new Vector3(startPos.x, startPos.y + heightDelta, startPos.z), fracComplete);
            yield return null;
        }
        go.SetActive(false);
        TrashSpawner.Instance.StartLevel();

        OnAnimationFinished?.Invoke();
    }
}
