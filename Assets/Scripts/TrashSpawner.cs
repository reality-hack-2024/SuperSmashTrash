using Oculus.Interaction.DebugTree;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum PowerUpType
{
    Bottle, MetalFork, MetalSpoon, MetalKnife, Mug, Bag, None
}
[System.Serializable]
public struct Trash
{
    public GameObject trashPrefab;
    public GameObject powerUpPrefab;
    public PowerUpType Type;
}


public class TrashSpawner : MonoBehaviour
{
    public static TrashSpawner Instance = new();
    public UnityEvent OnLevelChange = new();

    public float spawnDelay = 2;
    public float spawnRadius = 2;
    public float torqueSpeed = 10;
    public List<Trash> trashPrefabs;
    public TMPro.TMP_Text levelText;

    private bool isFalling = false;
    private float countdown;
    private float level = 0;
    private float levelTrashMax = 0;
    private Vector3 origin;
    private List<Trash> activeTrashPrefabs; //Prefabs that are being spawned
    private int totalTrashSpawned = 0;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    } //Singleton pattern

    void Start()
    {
        activeTrashPrefabs = trashPrefabs;
        origin = gameObject.transform.position;
        countdown = spawnDelay;
    }

    public void RemoveTrash(PowerUpType type)
    {
        foreach (Trash trash in activeTrashPrefabs)
        {
            if (trash.Type == type)
            {
                activeTrashPrefabs.Remove(trash);
                break;
            }
        }
    }

    void Update()
    {
        if (isFalling)
        {
            if (countdown > 0)
            {
                countdown -= Time.deltaTime;
            }
            else
            {
                SpawnTrash();
                totalTrashSpawned += 1;
            }
        }

        //Next level
        if (totalTrashSpawned == levelTrashMax && levelTrashMax > 0 && isFalling)
        {
            OnLevelChange?.Invoke();
            isFalling = false;
        }
    }
    public void StartLevel()
    {
        //UI
        level += 1;
        levelText.text = $"{level}";
        levelTrashMax += level * 10;

        //Start spawning again
        isFalling = true;
    }
    public void StartFalling() { isFalling = true; }

    void SpawnTrash()
    {
        //Calc position
        float offsetX = Random.Range(-spawnRadius, spawnRadius);
        float offsetZ = Random.Range(-spawnRadius, spawnRadius);

        //Get trash prefab
        int trashNum = Random.Range(0, activeTrashPrefabs.Count - 1);
        GameObject obj;

        //Not reusable
        if (activeTrashPrefabs[trashNum].Type == PowerUpType.None)
            obj = activeTrashPrefabs[trashNum].trashPrefab;
        else
        {
            int randNum = Random.Range(0, 10);
            if (randNum == 0)
                obj = activeTrashPrefabs[trashNum].powerUpPrefab;
            else
                obj = activeTrashPrefabs[trashNum].trashPrefab;

        }

        GameObject newObj = Instantiate(obj, new Vector3(origin.x + offsetX, origin.y, origin.z + offsetZ), Quaternion.identity);
        if (newObj.TryGetComponent(out Rigidbody rb))
        {
            Vector3 torque;
            torque.x = Random.Range(-torqueSpeed, torqueSpeed);
            torque.y = Random.Range(-torqueSpeed, torqueSpeed);
            torque.z = Random.Range(-torqueSpeed, torqueSpeed);
            rb.AddTorque(torque);

        }
        countdown = spawnDelay / level;
    }
}
