using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Appear : MonoBehaviour
{
    public float appearTime = 2.0f;
    public float heightDelta = 0.4f;
    public bool disappearing = false;

    public AudioSource welcomeVoice;

    private float startTime;
    private Vector3 startPos;
    private Vector3 endScale;

    private bool welcomeTriggered = false;

    // Start is called before the first frame update
    void Start()
    {
        startTime = Time.time;
        startPos = transform.position;
        endScale = transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {
        float fracComplete = (Time.time - startTime) / appearTime;
        transform.localScale = Vector3.Slerp(new Vector3(0, 0, 0), endScale, fracComplete);
        transform.localPosition = Vector3.Slerp(new Vector3(startPos.x, startPos.y, startPos.z), new Vector3(startPos.x, startPos.y + heightDelta, startPos.z), fracComplete);
        if(fracComplete >= 0.6 && !welcomeTriggered)
        {
            welcomeVoice.Play();
            welcomeTriggered = true;
        }
    }
}
