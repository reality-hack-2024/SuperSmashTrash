using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class PlayHitSound : MonoBehaviour
{
    public AudioSource groundHitSource;

    private bool soundSounded = false;
    private void OnTriggerEnter(Collider other)
    {
        if (soundSounded) return;
        if (other.gameObject.tag != "Bag")
        {
            groundHitSource.Play();
            soundSounded = true;
            DeactivateAfter();
        }
    }
    private async void DeactivateAfter()
    {
        await Task.Delay(1000);
        groundHitSource.clip = null;

    }
}
