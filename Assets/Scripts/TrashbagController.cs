using Oculus.Voice.Bindings.Android;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TrashbagController : MonoBehaviour
{
    [Header("Positioning")]
    public GameObject leftHand;
    public GameObject rightHand;

    [Header("UI")]
    public TMP_Text trashCollected;
    public GameObject bagFull;
    public Image capacityBar;
    public float trashCapacity = 10f;

    [Header("Sound")]
    public AudioSource reloadSoundSource;
    public AudioSource catchSoundSource;
    public List<AudioClip> catchSoundClips = new();

    [Header("Others")]
    public GameObject bagPrefab;
    public Transform fullBagsSpawnPoint;

    private float totalTrashCollected;
    private float itemsInBag = 0;
    private bool levelPaused = true, uiUpdated = false;
    void Start()
    {
        TrashSpawner.Instance.OnLevelChange.AddListener(TogggleCatchingTrash);
        AppearTrigger.OnAnimationFinished.AddListener(TogggleCatchingTrash);
        trashCollected.text = totalTrashCollected.ToString();
    }

    void Update()
    {
        transform.position = (leftHand.transform.position + rightHand.transform.position) / 2f;
        transform.localScale = new Vector3(Vector3.Distance(leftHand.transform.position, rightHand.transform.position), 0.01f, Vector3.Distance(leftHand.transform.position, rightHand.transform.position));

        // if bag is full
        if (itemsInBag == trashCapacity)
        {
            //Update UI
            if (!uiUpdated)
            {
                Debug.Log("bag full");
                bagFull.SetActive(true);
                uiUpdated = true;
            }

            // Reload bag when full if hands brought together
            if (Vector3.Distance(leftHand.transform.position, rightHand.transform.position) < 0.15)
            {
                itemsInBag = 0;
                reloadSoundSource.Play();
                Instantiate(bagPrefab, fullBagsSpawnPoint);

                //update ui
                bagFull.SetActive(false);
                uiUpdated = false;

            }
        }

        // Show capacity remaining in bag
        capacityBar.fillAmount = itemsInBag / trashCapacity;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (levelPaused) return;

        if (other.gameObject.CompareTag("Trash"))
        {
            if (itemsInBag < trashCapacity)
            {
                totalTrashCollected += 1;
                itemsInBag += 1;
                trashCollected.text = totalTrashCollected.ToString();
                other.gameObject.SetActive(false);

                int index = Random.Range(0, catchSoundClips.Count);
                catchSoundSource.clip = catchSoundClips[index];
                catchSoundSource.Play();
            }
        }
    }
    private void TogggleCatchingTrash()
    {
        levelPaused = !levelPaused;
    }

}
