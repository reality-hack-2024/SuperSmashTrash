using UnityEngine;


public class PowerUpTrigger : MonoBehaviour
{
    [SerializeField] private PowerUpType powerUp;

    private void OnTriggerEnter(Collider other)
    {
        TrashSpawner.Instance.RemoveTrash(powerUp);
    }
}
