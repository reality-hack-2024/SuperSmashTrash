using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioManager : MonoBehaviour
{
    [SerializeField] AudioClip gameMusic;
    [SerializeField] AudioClip idleMusic;
    [SerializeField] float speed = 0.1f;
    AudioSource ambientAudioSource;
    float startVolume;
    private void OnEnable()
    {
        AppearTrigger.OnAnimationFinished.AddListener(PlayAmbientSound); //Starts playing game music
        TrashSpawner.Instance.OnLevelChange.AddListener(PlayAmbientSound); //Starts idle music
        ambientAudioSource = GetComponent<AudioSource>();
        ambientAudioSource.clip = idleMusic;
        ambientAudioSource.loop = true;
        ambientAudioSource.Play();
        startVolume = ambientAudioSource.volume;
    }
    public void PlayAmbientSound()
    {
       StartCoroutine(ChangeAudioFading());

    }
    private IEnumerator ChangeAudioFading()
    {
        //fade out        
        while (ambientAudioSource.volume > 0)
        {
            ambientAudioSource.volume -= speed;

            yield return null;
        }

        //Change audio
        ambientAudioSource.clip = ambientAudioSource.clip == idleMusic ? gameMusic : idleMusic;
        ambientAudioSource.Play();
        //fade in
        while (ambientAudioSource.volume < startVolume)
        {
            ambientAudioSource.volume += speed;

            yield return null;
        }
        ambientAudioSource.volume = startVolume;
    }
}
