# Super Smash Trash

Super Smash Trash is a game built during the 2024 MIT Reality Hack. As part of the Living Harmony track, Super Smash Trash is designed to help people visualize the amount of waste they generate, and experience how seemingly minor decisions can compound over a great amount of people.

Featuring a physical controller that mirrors the one used in gameplay, Super Smash Trash is an experience that is immersive physically as well as through visuals and audio.

In this game, you'll literally see the impact of your daily trash in the most interactive way possible by utilizing the power of Mixed Reality! Grab your trusty physical controller: a trash bag - and try to catch the trash falling from the heavens, before it hits the ground. With each round, the trash increases by the amount of trash an additional person produces per day. How long can you survive in this infinite game mode?

You want to reduce the amount falling from the sky? Try to catch the powerups - these are reusable products which can replace the disposable products and stop them from being added to the trash. Some amount of waste is inevitable, but by using reusable items whenever possible, we can greatly reduce the amount of trash we generate. When your trash bag fills up, close it in real life to quickly reload - and add a full trash bag to your pile.

We were inspired at this hackathon by witnessing the mounds of single-use plastic overflowing the bins after every meal. To make tackling this issue more approachable and hopefully reach more people, we frame it in an entertaining and fun way. 

How can this change the world? By seeing the waste pile up in front of us, instead of being thrown away and forgotten, we more easily understand the impact of our choices. 

This is a game that you definitely can’t win - but if millions adopt mindful consumption in real life, we can turn the tide, reduce waste, and foster a better tomorrow in this shared home we call Earth.


## Setup

1. Install Unity
2. Install the ShapesXR plugin
3. Clone this repo
4. git-lfs pull

### Hardware Required

- Meta Quest 3
- Trash bag 

### Software Dependencies

- Unity 2022.3.11f1
- Universal Render Pipeline v14.0.9
- ShapesXR plugin (https://learn.shapesxr.com/sharing-and-export/unity-plugin)
- Meta XR SDK v60.0

## Run

1. Open the project in Unity
2. Press "run"
3. Put on the headset 
4. Equip your trash bag
5. Enjoy!

## Shout-Outs

We would like to thank these mentors for their help:

- Angel Muniz
- Craig Herndon
- Martin Sawtell
- Patrick Burton
- Wyatt Roi

We couldn't have done it without you!

## Licenses and Credits

### Music

- *(Ambient) Breath* by Kirk Osamayo, source: Free Music Archive, license: CC BY
- *Don't Bother Me* *MAGA Man* by John Lopker | Popular USA Majority; source: Free Music Archive, license: CC BY

### Sound effects
- *Plastic, Impact, Hit, Boat SND0740* - Soundly free library, https://getsoundly.com/
- *Cartoon, Musical, Mystery, Musical Tone 01 SND15332* - Soundly free library, https://getsoundly.com/
- *Cartoon, Horn, Clown, Comical, Honk SND11773* Soundly free library, https://getsoundly.com/
- *Cartoon, Boing, Jump, Spring* - Soundly free library, https://getsoundly.com/
- *Guns, Mechanism, Shotgun, Cock, Reload, Lock 'n Load SND6290* - Soundly free library, https://getsoundly.com/
- *User Interface, Beep, Button, Happy, Select, Confirm, Deselect, Cancel* -  Soundly free library, https://getsoundly.com/
- *Plastic, Impact, Hit, Boat SND0740* - Soundly free library, https://getsoundly.com/
- *Musical, Misc, Computer, OS Modern, Celestial, Heavenly, Pads, Bells SND64163 3* - Soundly free library, https://getsoundly.com/
- *Cartoon, Musical, Dreamy, Heavenly, Glissando, Harp, Choir SND15188* - Soundly free library, https://getsoundly.com/


### 3D Assets

- *Food Kit* - https://www.kenney.nl/assets/food-kit; license: CC0
- *Canvas Tote Bag* - Adobe Stock free - https://stock.adobe.com/3d-assets/canvas-tote-bag/209726949?content_id=209726949&asset_id=209726949 - licensed
- *Fork_Spoon_Knife Set MAX 2011* - https://www.turbosquid.com/3d-models/fork_spoon_knife-set-max-2011-711459 - Standard 3D Model License - free
- *CRYSTAL_GEYSER* - https://www.turbosquid.com/3d-models/free-crystal-geyser-3d-model/325962 - Standard 3D Model License - free
- *Low Poly Shopping Bag* - https://www.turbosquid.com/3d-models/low-poly-shopping-bag-1714122 - Standard 3D Model License - free
- *Paper Coffee Cup* - https://www.turbosquid.com/3d-models/paper-coffee-cup-1487930 - Standard 3D Model License - free
- *Low Poly Mugs* - https://www.turbosquid.com/3d-models/low-poly-mugs-1194136 - Standard 3D Model License - free
- *Paper Bag* - https://www.turbosquid.com/3d-models/paper-bag-1190189 - Standard 3D Model License - free
- *Metal Can* - https://www.turbosquid.com/3d-models/metal-can-1960980 - Standard 3D Model License - free
- *Cardboard Box* - https://www.turbosquid.com/3d-models/cardboard-box-1845866 - Standard 3D Model License - free
- *Trash Bag* - Generated using Meshy
- *Crumpled Paper Ball* - Generated using Meshy
- *Reusable water bottle* - Generated using Meshy

### Fonts
- *https://fonts.google.com/specimen/Honk, license OFL open font licence
- *https://fonts.google.com/specimen/Montserrat, license OFL open font licence

## Team
- Roshan Mohan
- Anna Buchele
- Markus Sauerbeck
- Laura Magdaleno Amaro
- Phillip Cherner

## Tracks
Purpose Track
- Living Harmony 

Other Tracks
- Social XR - All Together Now
- Best Use of ShapesXR
- Creative Concepts with Meta Presence Platform

